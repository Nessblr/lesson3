import UIKit

class Home {
    
    let priceHome: Double
    let spaceHome: Int
    let windowsHome: Int
    let numberHome: Int
    
    init(price: Double, space: Int, windows: Int, numberHome: Int) {
        self.priceHome = price
        self.spaceHome = space
        self.windowsHome = windows
        self.numberHome = numberHome
    }
    
    func getHomeInfo() {
        print("Цена дома: \(priceHome), Количество комнат дома: \(spaceHome), Количество окон дома: \(windowsHome), Номер дома: \(numberHome)")
    }
    
  
   
}

var objectHome = Home(price: 900, space: 11, windows: 20, numberHome: 7 )


var arrayHome = [objectHome]

arrayHome.append(Home(price: 2000, space: 6, windows: 6, numberHome: 21 ))
arrayHome.append(Home(price: 1000, space: 4, windows: 8, numberHome: 33 ))
arrayHome.append(Home(price: 500, space: 7, windows: 10, numberHome: 78 ))

//arrayHome.forEach({$0.getHomeInfo()})

let filterHome = arrayHome.filter({$0.priceHome >= 1000})

//filterHome.forEach({$0.getHomeInfo()})

print("=========================HOME WORK===================================")
//Создайте класс Car у которого будут следующие поля: модель, скорость, вес, цена, расход по километражу. Все поля кроме модели задать рандомно, можно использовать класс Int.random.  Создать 4 машины и найти какая среди машин будет самая быстрая, самая дешёвая. Дать возможность юзеру назначить дистанцию(совершить гонку) и получить среди всех машин ту которая проедет бысстрее всех(учитывая все их характеристики), ту которая проедет медленнее всех, ту у которой первой закончится бензин. Показать список после гонки в формате 1) место - и тд. Результат продемонстрировать в консоли.

//Создать структуру Человек с полями сила левой руки, сила правой руки, сила левой ноги, сила правой ноги, cила удара головой(быкана). Создать 20 человек, устроить драку таким образом: сначало первый человек дерётся со вторым, если побеждает то с третьим и тд, в случае если проигрывает то победивший продолжает путь и сражается со следующим. Характеристики задавать рандомно через рендж. Правила боя:

//Всего делается 3 удара по очереди, сначало бьет один боец, потом второй и тд по очереди 3 раза. Побеждает тот кто нанёс больше урона(усложнённо, сделать приоритет: голова самое большое количество урона, и тд). Результат продемонстрировать в консоли.


class Car {
    
    let modelCar: String
    let speedCar: Int
    let weightCar: Int
    let priceCar: Int
    let rashodPoKmCar: Int
    
    init(modelCar: String, speedCar: Int, weightCar: Int, priceCar: Int, rashodPoKmCar: Int) {
        self.modelCar = modelCar
        self.speedCar = speedCar
        self.weightCar = weightCar
        self.priceCar = priceCar
        self.rashodPoKmCar = rashodPoKmCar
    }
    
    func getCarInfo() {
        print("Модель машины: \(modelCar), Скорость машины: \(speedCar), Вес машины: \(weightCar), Цена машины: \(priceCar), Расход по километражу: \(rashodPoKmCar)")
    }
    
}

var objectCar = Car(modelCar: "Opel", speedCar: Int.random(in: 10...100), weightCar: Int.random(in: 10...100), priceCar: Int.random(in: 10...100), rashodPoKmCar: Int.random(in: 10...100))

var arrayCar = [objectCar]

arrayCar.append(Car(modelCar: "Audi", speedCar: Int.random(in: 10...100), weightCar: Int.random(in: 10...100), priceCar: Int.random(in: 10...100), rashodPoKmCar: Int.random(in: 10...100)))
arrayCar.append(Car(modelCar: "Nissan", speedCar: Int.random(in: 10...100), weightCar: Int.random(in: 10...100), priceCar: Int.random(in: 10...100), rashodPoKmCar: Int.random(in: 10...100)))
arrayCar.append(Car(modelCar: "Mazda", speedCar: Int.random(in: 10...100), weightCar: Int.random(in: 10...100), priceCar: Int.random(in: 10...100), rashodPoKmCar: Int.random(in: 10...100)))

arrayCar.forEach({$0.getCarInfo()})


print("===========FILTRED SPEED======================")

let filterSpeedCar = arrayCar.filter({$0.speedCar > 70})

filterSpeedCar.forEach({$0.getCarInfo()})

print("===========FILTRED PRICE======================")

let filterPriceCar = arrayCar.filter({$0.priceCar > 70})

filterPriceCar.forEach({$0.getCarInfo()})

print("=================================")

func driveCar(distance: Int, cars: [Car]) {
        
    let fastCar = cars.sorted(by: {$0.speedCar > $1.speedCar})
    
    let slowCar = cars.sorted(by: {$0.speedCar < $1.speedCar})
    
    var arrayTupleCar: [(Car, Int)] = []
    
   cars.forEach({ car in
        
        let benzinCar = distance * car.rashodPoKmCar
        
        var kmOnMin = car.speedCar / 60
        
        let resultOnSecCar = distance / kmOnMin
        
        if car.kolvoBenzina >= benzinCar {
            
            arrayTupleCar.append((car,resultOnSecCar))
            
        } else {print( "Не учавствует в гонке: ", (car.modelCar))}
        
       
    })
    
    var resultRace = arrayTupleCar.sort(by: {$0.1 < $1.1})
    
    arrayTupleCar.forEach{car, sec in print(car.modelCar, sec)}
}

driveCar(distance: 22, cars: arrayCar)

print("==================================")




print("==================================")

//Создать структуру Человек с полями сила левой руки, сила правой руки, сила левой ноги, сила правой ноги, cила удара головой(быкана). Создать 20 человек, устроить драку таким образом: сначало первый человек дерётся со вторым, если побеждает то с третьим и тд, в случае если проигрывает то победивший продолжает путь и сражается со следующим. Характеристики задавать рандомно через рендж. Правила боя:

//Всего делается 3 удара по очереди, сначало бьет один боец, потом второй и тд по очереди 3 раза. Побеждает тот кто нанёс больше урона(усложнённо, сделать приоритет: голова самое большое количество урона, и тд). Результат продемонстрировать в консоли.



struct HumanStruct {
    
    let leftHandPower: Int
    let rightHandPower: Int
    let leftLegPower: Int
    let rightLegPower: Int
    let headPower: Int
    
    
    func getHumanInfo() {
        print("Сила левой руки: \(leftHandPower), Сила правой руки: \(rightHandPower), Сила левой ноги: \(leftLegPower), Сила правой ноги: \(rightLegPower), Сила удара головой: \(headPower)")
    }
}


var arrayHuman: [HumanStruct] = []

arrayHuman.append(HumanStruct(leftHandPower: 10, rightHandPower: 25, leftLegPower: 15, rightLegPower: 20, headPower: 25))
arrayHuman.append(HumanStruct(leftHandPower: 20, rightHandPower: 30, leftLegPower: 25, rightLegPower: 25, headPower: 35))
arrayHuman.append(HumanStruct(leftHandPower: 15, rightHandPower: 20, leftLegPower: 35, rightLegPower: 30, headPower: 45))
arrayHuman.append(HumanStruct(leftHandPower: 5, rightHandPower: 15, leftLegPower: 20, rightLegPower: 15, headPower: 55))
arrayHuman.append(HumanStruct(leftHandPower: 30, rightHandPower: 5, leftLegPower: 5, rightLegPower: 5, headPower: 45))

arrayHuman.forEach({$0.getHumanInfo()})

func FightHuman() {
    
    
    arrayHuman.forEach({ human in
        
        var powerHuman = human.leftHandPower + human.rightHandPower + human.leftLegPower + human.rightLegPower + human.headPower
    
        print("Сила человека: \(powerHuman)")
        
        
        
    })
    
    
}

FightHuman()
